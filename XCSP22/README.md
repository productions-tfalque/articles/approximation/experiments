# Approximate and solve


A campaign about approximation method for constraint optimization problem. 


## Description of the environment

All solvers have been run on a cluster of computers equipped with $`128`$ GB of RAM and two quadcore Intel XEON E5-2637 ($`3.5`$ GHz).
The time limit was set to $`40`$ minutes and the memory limit to $`32`$ GB.

## Description of the input instances

We used all COP instances from the [XCSP22 competition](https://www.cril.univ-artois.fr/XCSP22/competitions/cop/cop).

## Analysis

We now present an analysis of our experiments using
[*Metrics*](https://github.com/crillab/metrics), which has been used to
generate this report.
You can browse the notebooks of this analysis from the following table of
contents:

- [Loading Experiment Data](./load_experiments.ipynb)
- [Tables](./table_analysis.ipynb)
