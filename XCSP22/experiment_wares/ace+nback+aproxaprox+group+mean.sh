#!/bin/bash

# This script executes the program being experimented.
#
# $1 - The job to execute.
# $2 - The output directory.

dir=$(dirname $0)
export PATH="$PATH:$dir/../tools"
lzma --decompress --stdout $1 > $2/instance.xml
/usr/java11/jdk-11.0.1/bin/java -Xms25000M -Xmx25000M -cp "$dir/ACE.jar:$dir/aceurancetourix-0.1.9.jar:$dir/approximation-0.1.0.jar:$dir/argparse4j-0.9.0.jar:$dir/juniverse-0.1.11.jar" fr.univartois.cril.approximation.Main -i "$2/instance.xml" --path-strategy APPROX_APPROX --mean true --measure NEffectiveBacktracking
