#!/bin/bash

# This script executes the program being experimented.
#
# $1 - The job to execute.
# $2 - The output directory.

dir=$(dirname $0)
export PATH="$PATH:$dir/../tools"
/usr/java11/jdk-11.0.1/bin/java -Xms25000M -Xmx25000M -jar "$dir/ACE-2.1.jar" "$1" -npc=True -ev   
