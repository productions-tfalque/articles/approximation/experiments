# Approximate and solve

## Abstract 

In this paper, we introduce sub-approximation methods to improve the solving of constraint satisfaction (CSP) and contraint optimization (COP) problems.
In particular, we propose to use different measures to select constraints to remove, so as to reduce the problem until it becomes easy.
The solution that is found for the sub-problem is then used to guide the search on the original problem.
We evaluate the performance of our method on a set of problems from recent contraint programming competitions, and compare the results with classical solving approaches.
The obtained results show that our approach improves the performance of the solver, even allowing to find solutions for problems that are not otherwise solved.

## Summary 

![XCSP22](./XCSP22)

## Authors

- Falque Thibault
- Lagniez Jean-Marie
- Lecoutre Christophe
- Wallon Romain
